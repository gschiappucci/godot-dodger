extends Area2D

onready var shieldTimer = get_node("shieldTimer")
onready var shield = get_node("shield")
onready var shieldTween = get_node("shield/shieldTween")

export var speed = 100
var rotationSpeed = 0.15
var screenSize
var ufoSize
var shields = true
signal playerDestroyed

func _ready():
	screenSize = get_viewport_rect().size
	ufoSize = get_node("sprite").get_item_rect().size / 2
	set_process(true)

func _process(delta):
	var input = Vector2(0,0)
	input.x = Input.is_action_pressed("ui_right") - Input.is_action_pressed("ui_left")
	input.y = Input.is_action_pressed("ui_down") - Input.is_action_pressed("ui_up")
	var movement = get_pos() + input.normalized() * speed * delta
	movement.x = clamp(movement.x, ufoSize.x, screenSize.x - ufoSize.x)
	movement.y = clamp(movement.y, ufoSize.y, screenSize.y - ufoSize.y)
	set_pos(movement)
	set_rot(get_rot() + rotationSpeed)

func _on_ufo_area_enter( area ):
	var cometSize = area.get_scale().x
	if !shields or cometSize > 0.65:
		gameOver()
	else:
		shieldsDown()

func shieldsDown():
	shieldTween.interpolate_property(shield,"visibility/opacity",1,0,0.4,Tween.TRANS_LINEAR, Tween.EASE_IN)
	shieldTween.interpolate_property(shield,"transform/scale",Vector2(1,1),Vector2(1.5,1.5),0.4,Tween.TRANS_EXPO, Tween.EASE_OUT)
	shieldTween.start()
	shieldTimer.start()
	shields = false

func gameOver():
	emit_signal("playerDestroyed")
	queue_free()

func _on_shieldTimer_timeout():
	shield.show()
	shieldTween.interpolate_property(shield,"visibility/opacity",0,1,0.5,Tween.TRANS_LINEAR, Tween.EASE_IN)
	shieldTween.start()
	shields = true

func _on_shieldTween_tween_complete( object, key ):
	shield.set_scale(Vector2(1,1))
