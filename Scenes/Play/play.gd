extends Node

onready var asteroid = preload("res://Asteroid/asteroid.tscn")
onready var star = preload("res://Star/star.tscn")
onready var asteroidContainer = get_node("asteroidContainer")
onready var asteroidTimer = get_node("asteroidTimer")
onready var starContainer = get_node("starContainer")
onready var scoreLabel = get_node("HUD/scoreLabel")

var spawn = true
var screenSize
var score = 0

func _ready():
	randomize()

	get_node("ufo").connect("playerDestroyed", self, "playerDestroyed")
	screenSize = get_viewport().get_rect().size
	
	for i in range(50):
		spawnStar(rand_range(0,screenSize.y))
	
	set_process(true)

func _process(delta):
	if starContainer.get_child_count() < 50:
		spawnStar(0)
	if (!spawn):
		asteroidTimer.stop()
	else:
		score += 1
		scoreLabel.set_text("Score: " + str(score))

func spawnAsteroid():
	var newAsteroid = asteroid.instance()
	newAsteroid.set_pos(Vector2(rand_range(0,screenSize.x),-10))
	asteroidContainer.add_child(newAsteroid)

func spawnStar(yPosition):
	var newStar = star.instance()
	newStar.set_pos(Vector2(rand_range(0,screenSize.x),yPosition))
	starContainer.add_child(newStar)

func playerDestroyed():
	spawn = false

func _on_asteroidTimer_timeout():
	spawnAsteroid()

func _on_saveScoreButton_pressed():
	pass # replace with function body
