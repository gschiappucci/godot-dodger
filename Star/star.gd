extends Sprite

var speed = 5
var screenSize
var starSize

func _ready():
	screenSize = get_viewport_rect().size
	starSize = self.get_item_rect().size
	set_process(true)

func _process(delta):
	var movement = get_pos() +  Vector2(0.0, speed * delta)
	set_pos(movement)
	if movement.y > screenSize.y + starSize.y:
		queue_free()
