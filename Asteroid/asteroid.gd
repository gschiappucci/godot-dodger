extends Area2D

onready var sprite = get_node("sprite")
var speed = 350
var screenSize
var asteroidSize
var rotationSpeed

func _ready():
	randomize()
	
	screenSize = get_viewport_rect().size
	asteroidSize = sprite.get_item_rect().size
	rotationSpeed = rand_range(0.02, 0.12)
	speed = rand_range(300,600)
	
	var randScale = rand_range(0.15, 1)
	set_scale(Vector2(randScale,randScale))
	
	sprite.set_frame(randi() % 4)
	
	set_process(true)

func _process(delta):
	var movement = get_pos() +  Vector2(0.0, speed * delta)
	set_pos(movement)
	
	set_rot(get_rot() + rotationSpeed)
	
	if movement.y > screenSize.y + asteroidSize.y:
		queue_free()

func _on_asteroid_area_enter( area ):
	if area.get_name() == "ufo":
		queue_free()
